/*
 * Production Webpack Config
 */

import TerserJsPlugin from 'terser-webpack-plugin';
import commonConfig from './webpack.config.common';

module.exports = {
  ...commonConfig,
  mode: 'production',
  optimization: {
    ...commonConfig.optimization,
    minimizer: [
      new TerserJsPlugin({
        parallel: true,
      }),
    ],
  },
};

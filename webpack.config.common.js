/*
 * Common Webpack Config
 */

import webpack from 'webpack';
import path from 'path';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

import packageJSON from './package.json';

module.exports = {
  entry: [
    path.join(__dirname, './app/index'),
  ],
  output: {
    path: path.join(__dirname, './static'),
    publicPath: '/',
    filename: 'bundle.js',
    libraryTarget: 'commonjs2',
  },
  mode: 'development',
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new MiniCssExtractPlugin({
      filename: 'app.css',
      allChunks: true,
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        VERSION: JSON.stringify(packageJSON.version),
      },
    }),
    new webpack.IgnorePlugin(/jsdom$/),
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loaders: 'babel-loader',
        include: path.join(__dirname, 'app'),
        exclude: /node_modules/,
      },
      {
        // activate source maps via loader query
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|woff|woff2|eot|ttf)$/,
        loader: 'url-loader?limit=100000',
      },
      {
        test: /\.jpg$/,
        loader: 'file-loader',
      },
      {
        test: /\.css$/,
        include: [
          path.resolve('./node_modules/react-datepicker'),
          path.resolve('./node_modules/react-draft-wysiwyg'),
          path.resolve('./node_modules/react-sweet-progress'),
        ],
        loader: 'style-loader!css-loader',
      },
      {
        test: /\.(xml|manifest|svg|ico)/,
        loader: 'file-loader',
      },
      {
        test: /assets\/favicons\/.*$/,
        loader: 'url-loader',
        query: { limit: 1, name: 'favicons/[name].[ext]' },
      },
    ],
  },
  externals: {
    react: 'commonjs react',
    'redux-form': 'redux-form',
  },
  resolve: {
    modules: [
      'app',
      'node_modules',
    ],
    unsafeCache: true,
    extensions: [
      '.js',
      '.jsx',
      '.manifest',
      '.css',
      '.scss',
    ],
  },
};

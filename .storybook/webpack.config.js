const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin  = require('mini-css-extract-plugin');

module.exports = async ({ config, mode }) => {
  config.plugins.push(
    new webpack.NoEmitOnErrorsPlugin(),
    new MiniCssExtractPlugin({
      filename: 'app.css',
      allChunks: true,
    }),
    new webpack.IgnorePlugin(/jsdom$/),
  );
  config.module.rules.push(
      {
        test: /\.(js|jsx)$/,
        loaders: 'babel-loader',
        include: path.join(__dirname, 'app'),
        exclude: /node_modules/,
      },
      {
        // activate source maps via loader query
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|woff|woff2|eot|ttf)$/,
        loader: 'url-loader?limit=100000',
      },
      {
        test: /\.jpg$/,
        loader: 'file-loader',
      },
      {
        test: /\.css$/,
        include: [
          path.resolve('../node_modules/react-datepicker'),
          path.resolve('../node_modules/react-draft-wysiwyg'),
          path.resolve('../node_modules/react-sweet-progress'),
        ],
        loader: 'style-loader!css-loader',
      },
      {
        test: /\.(xml|manifest|svg|ico)/,
        loader: 'file-loader',
      },
      {
        test: /assets\/favicons\/.*$/,
        loader: 'url-loader',
        query: { limit: 1, name: 'favicons/[name].[ext]' },
      },
      {
        test: /\.stories\.jsx?$/,
        loaders: [require.resolve('@storybook/addon-storysource/loader')],
        enforce: 'pre',
      }
    );

  // Return the altered config
  return config;
};

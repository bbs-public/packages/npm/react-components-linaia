import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import FieldError from "./FieldError";

const FieldLayout = ({
  input,
  meta,
  id,
  fieldClassName,
  label,
  sublabel,
  sublabelClassName,
  fontIcon,
  disabled,
  childComponent: ChildComponent,
  ...props
}) => (
  <div
    className={classNames("field " + fieldClassName, { "read-only": disabled })}
  >
    <FieldError {...meta} />
    <ChildComponent input={input} id={id} disabled={disabled} {...props} />
    {label && (
      <label htmlFor={id || (input && input.name)}>
        {fontIcon && <i className={classNames(fontIcon)} />}
        {label}
        {sublabel && (
          <div className={classNames("sublabel " + sublabelClassName)}>
            {sublabel}
          </div>
        )}
      </label>
    )}
  </div>
);

FieldLayout.propTypes = {
  input: PropTypes.shape(),
  meta: PropTypes.shape().isRequired,
  id: PropTypes.string,
  fieldClassName: PropTypes.string,
  sublabelClassName: PropTypes.string,
  label: PropTypes.string,
  sublabel: PropTypes.string,
  fontIcon: PropTypes.string,
  disabled: PropTypes.bool,

  childComponent: PropTypes.func.isRequired
};

export default FieldLayout;

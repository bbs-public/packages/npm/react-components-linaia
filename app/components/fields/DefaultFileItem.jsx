import React from "react";
import PropTypes from "prop-types";

const DefaultFileItem = ({ item, disabled, isBase64, getUrl, onClickRemove }) => {
  let fileType =
    item.includes(".gif") ||
    item.includes(".jpeg") ||
    item.includes(".jpg") ||
    item.includes(".png") ||
    item.includes(".svg+xml");

  let fileTypeDoc =
    item.includes(".pdf") ||
    item.includes(".docx") ||
    item.includes(".doc") ||
    item.includes(".xls") ||
    item.includes(".x-xls") ||
    item.includes(".csv") ||
    item.includes(".xml") ||
    item.includes(".plain") ||
    item.includes(".pptx") ||
    item.includes(".vnd.ms-excel") ||
    item.includes(".msexcel") ||
    item.includes(".x-ms-excel") ||
    item.includes(".x-msexcel") ||
    item.includes(".x-excel") ||
    item.includes(".x-dos_ms_excel") ||
    item.includes(".vnd.openxmlformats-officedocument.spreadsheetml.sheet");

  if (fileType) {
    const typeImg = item.split(".").pop();
    return (
      <div className="img-block">
        {isBase64 ? (
          <img alt={item.url} src={item.url} />
        ) : (
          <a href={getUrl ? getUrl(item) : item} target="_blank" rel="noopener noreferrer">
            <img alt={item} src={getUrl ? getUrl(item) : item} />
          </a>
        )}

        {!disabled && (
          <span className="btn-img" onClick={() => onClickRemove()}>
            <i className="far fa-times" />
          </span>
        )}
      </div>
    );
  } else if (fileTypeDoc) {
    const typeFile = item.split(".").pop();
    return (
      <div className="file-block">
        {isBase64 ? (
          <img alt={item.url} src={item.url} />
        ) : (
          <a href={getUrl ? getUrl(item) : item} target="_blank" rel="noopener noreferrer">
            <i className={`file-icon file-icon-${typeFile}`}></i>
            <span>{item}</span>
          </a>
        )}
        {!disabled && (
          <span className="btn-img" onClick={() => onClickRemove()}>
            <i className="far fa-times" />
          </span>
        )}
      </div>
    );
  } else {
    <a href={getUrl ? getUrl(item) : item} target="_blank" rel="noopener noreferrer">
      <img alt={item} src={getUrl ? getUrl(item) : item} />
    </a>;
  }
};

DefaultFileItem.propTypes = {
  item: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.shape({
      url: PropTypes.string.isRequired
    }).isRequired
  ]).isRequired,
  disabled: PropTypes.bool,
  isBase64: PropTypes.bool,
  getUrl: PropTypes.func,
  onClickRemove: PropTypes.func.isRequired
};

export default DefaultFileItem;

import React from 'react';
import PropTypes from 'prop-types';

import { getOptions } from '../../utils/OptionsParser';

const MultiCheckboxField = ({
  input, id, classNameOption, disabled, options, optionsKeys, classNameGroup,
}) => (
  <div className={classNameGroup || 'fields-group inline multiCheckbox'}>
    {getOptions(options, optionsKeys).map(({ value, label: optionLabel }) => (
      <div key={value} className={classNameOption}>
        <input
          {...input}
          value={value}
          onChange={(e) => {
            const values = input.value || [];
            if (e.target.checked === false) {
              return input.onChange(values.filter(v => v !== value));
            }
            return input.onChange(values.concat(e.target.value));
          }}
          id={`multi_cb_${id || (input && input.name)}_${value}`}
          disabled={disabled ? 'disabled' : ''}
          type="checkbox"
          checked={(input.value || []).includes(value)}
        />
        <label
          className="label-option"
          htmlFor={`multi_cb_${id || (input && input.name)}_${value}`}
        >
          {optionLabel}
        </label>
      </div>
    ))}
  </div>
);

MultiCheckboxField.propTypes = {
  input: PropTypes.shape().isRequired,
  id: PropTypes.string,
  disabled: PropTypes.bool,

  classNameGroup: PropTypes.string,
  classNameOption: PropTypes.string,
  options: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({
      value: PropTypes.oneOfType([
        PropTypes.string.isRequired,
        PropTypes.number.isRequired,
        PropTypes.shape().isRequired,
      ]),
      label: PropTypes.string.isRequired,
    })).isRequired,
    PropTypes.objectOf(PropTypes.string.isRequired),
  ]).isRequired,
  optionsKeys: PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  }),
};

export default MultiCheckboxField;

import React from 'react';
import PropTypes from 'prop-types';

const ToggleField = ({
  input, id, disabled,
}) => (
  <input
    {...input}
    id={id || (input && input.name)}
    disabled={disabled ? 'disabled' : ''}
    type="checkbox"
    checked={input.value === true}
  />
);

ToggleField.propTypes = {
  input: PropTypes.shape().isRequired,
  id: PropTypes.string,
  disabled: PropTypes.bool,
};

export default ToggleField;

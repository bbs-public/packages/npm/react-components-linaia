import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { FieldArray } from 'redux-form';
import classNames from 'classnames';

import FieldError from './FieldError';

const ArrayField = ({
  name, id, className, label, fontIcon, disabled, component: FieldsComponent, removable, addLabel,
}) => (
  <FieldArray
    name={name}
    component={({ input, meta, fields }) => (
      <div className={classNames(className, { 'read-only': disabled })}>
        <FieldError {...meta} />
        { fields.map((field, index) => (
          <Fragment key={field}>
            <FieldsComponent
              field={field}
              index={index}
              onClickRemove={removable ? () => fields.remove(index) : null}
            />
          </Fragment>
        ))}
        {addLabel && (
          <div className="btn-group">
            <button type="button" className="btn btn-primary btn-small" onClick={() => fields.push()}>{addLabel}</button>
          </div>
        )}
        {label && (
          <label htmlFor={id || (input && input.name)}>
            {fontIcon && <i className={classNames(fontIcon)} />}
            {label}
          </label>
        )}
      </div>
    )}
  />
);

ArrayField.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string,
  className: PropTypes.string,
  label: PropTypes.string,
  fontIcon: PropTypes.string,
  disabled: PropTypes.bool,

  component: PropTypes.func.isRequired,
  removable: PropTypes.bool,
  addLabel: PropTypes.string.isRequired,
};

export default ArrayField;

import React from 'react';
import PropTypes from 'prop-types';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, ContentState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { find } from "lodash";

function textToEditorState(text = '') {
  const contentState = ContentState.createFromBlockArray(
    htmlToDraft(text).contentBlocks,
  );
  return EditorState.createWithContent(contentState);
}

function editorStateToText(editorState) {
  return draftToHtml(convertToRaw(editorState.getCurrentContent()));
}

const embedVideoCallBack = (link) =>{
  if (link.indexOf("youtube") >= 0){
      link = link.replace("watch?v=","embed/");
      link = link.replace("/watch/", "/embed/");
      link = link.replace("youtu.be/","youtube.com/embed/");
  }
  return link
}

export default class WysiwygTextarea extends React.PureComponent {
  static propTypes = {
    text: PropTypes.string,
    placeholder: PropTypes.string,
    onBlurProps: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(PropTypes.string),
    toolbarOptions: PropTypes.shape(),
  };

  constructor(props) {
    super();
    this.state = {
      editorState: textToEditorState(props.text),
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ editorState: textToEditorState(nextProps.text) });
  }

  onBlur() {
    this.props.onBlurProps(editorStateToText(this.state.editorState));
  }

  render() {
    const { options, placeholder, toolbarOptions } = this.props;
    const { editorState } = this.state;

    return (
      <Editor
        editorState={editorState}
        onEditorStateChange={e => this.setState({ editorState: e })}
        toolbarHidden={options && options.length === 0}
        onBlur={() => this.onBlur()}
        placeholder={placeholder}
        toolbar={ find(options, option => option === "embedded") ? {
          options: options || ['inline', 'colorPicker'],
          inline: {
            options: ['bold', 'italic', 'underline'],
          },
          embedded: { embedCallback: embedVideoCallBack },
          ...toolbarOptions,
        } : {
          options: options || ['inline', 'colorPicker'],
          inline: {
            options: ['bold', 'italic', 'underline'],
          },
          ...toolbarOptions,
        }}
      />
    );
  }
}

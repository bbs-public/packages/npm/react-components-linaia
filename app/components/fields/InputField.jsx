import React from "react";
import PropTypes from "prop-types";

const InputField = ({
  input,
  id,
  disabled,
  placeholder,
  type,
  autoComplete = true,
  ...props
}) => (
  <input
    {...input}
    {...props}
    onChange={
      type === "number"
        ? e => input.onChange(Number(e.target.value))
        : input.onChange
    }
    onBlur={
      type === "number"
        ? e => input.onBlur(Number(e.target.value))
        : input.onBlur
    }
    id={id || (input && input.name)}
    disabled={disabled ? "disabled" : ""}
    placeholder={placeholder}
    type={type || "text"}
    autoComplete={autoComplete ? "on" : "off"}
  />
);

InputField.propTypes = {
  input: PropTypes.shape().isRequired,
  id: PropTypes.string,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  autoComplete: PropTypes.bool
};

export default InputField;

import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { SketchField, Tools } from 'react-sketch';

export default class FabricJSField extends React.PureComponent {
  static propTypes = {
    input: PropTypes.shape().isRequired,
    disabled: PropTypes.bool,

    height: PropTypes.string,
  };

  constructor (props) {
    super();
    this.value = props.input.value;
  }

  autoSave (value) {
    const { input: { onChange } } = this.props;
    if (JSON.stringify(this.value) !== JSON.stringify(value)) {
      this.value = value;
      clearTimeout(this.saveTimeout);
      this.saveTimeout = setTimeout(() => {
        onChange(value);
      }, 500);
    }
  }

  render () {
    const {
      input, disabled, height = '160px',
    } = this.props;

    return (
      <Fragment>
        <SketchField
          className="canvas-block"
          value={input.value}
          ref={(c) => { this.sketchCanvas = c; }}
          height={height}
          tool={Tools.Pencil}
          color="black"
          lineWidth={4}
          onChange={() => (!disabled ? this.autoSave(this.sketchCanvas.toJSON()) : null)}
        />
        {!disabled && (
          <button className="btn btn-small" onClick={() => input.onChange({})}>Effacer</button>
        )}
      </Fragment>
    );
  }
}

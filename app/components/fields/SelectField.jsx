import React from "react";
import PropTypes from "prop-types";
import Select from "react-select";
import Creatable from "react-select/lib/Creatable";
import { each, map, keyBy, capitalize, sortBy } from "lodash";

import { isUserAgentMobile } from "../../utils/DeviceUtils";
import { getOptions } from "../../utils/OptionsParser";

const SelectField = ({
  input,
  id,
  disabled,
  removable,
  placeholder,
  options,
  optionsKeys,
  creatable,
  isMulti = false,
  disableSortByValue,
  classNameReactSelect = "react-select",
  classNamePrefix,
  noOptionsAvailable,
  ...otherProps
}) => {
  let optArray = [];
  if (disableSortByValue) {
    optArray = getOptions(options, optionsKeys);
  } else {
    optArray = sortBy(getOptions(options, optionsKeys), (opt) =>
      capitalize(opt.label)
    );
  }
  const optObject = keyBy(optArray, "value");
  const newPlaceholder =
    placeholder || (creatable ? "Créer / Sélectionner..." : "Sélectionner...");
  let SelectCompnent = Select;
  let formatCreateLabel = (inputValue) => <span>{inputValue}</span>;
  let SelectComponent = Select;
  if (creatable) {
    SelectComponent = Creatable;
    const pushCreatedValue = (v) => {
      optArray.push({ label: v, value: v });
      optObject[v] = { label: v, value: v };
    };
    if (isMulti) {
      each(input.value, (v) => optObject[v] || pushCreatedValue(v));
    } else if (input.value && !optObject[input.value]) {
      pushCreatedValue(input.value);
    }
  }
  console.log("value of input : ", input);
  return !isMulti && !creatable && isUserAgentMobile ? (
    <select
      {...input}
      id={id || (input && input.name)}
      disabled={disabled}
      placeholder={newPlaceholder}
      isClearable={removable}
      {...otherProps}
    >
      <option value="" disabled selected hidden />
      {map(optArray, ({ value, label: optionLabel }) => (
        <option key={value} value={value}>
          {optionLabel}
        </option>
      ))}
    </select>
  ) : (
    <SelectComponent
      value={
        isMulti === true
          ? !isEmpty(map(input.value, (v) => optObject[v]))
            ? map(input.value, (v) => optObject[v])
            : null
          : optObject[input.value] || null
      }
      onChange={(v) =>
        isMulti === true
          ? input.onChange(map(v, "value"))
          : input.onChange(v ? v.value : null)
      }
      id={id || (input && input.name)}
      isDisabled={disabled}
      isClearable={removable}
      placeholder={newPlaceholder}
      options={optArray}
      isMulti={isMulti}
      className={classNameReactSelect}
      classNamePrefix={classNamePrefix}
      formatCreateLabel={noOptionsAvailable}
      {...otherProps}
    />
  );
};

SelectField.propTypes = {
  input: PropTypes.shape().isRequired,
  id: PropTypes.string,
  disabled: PropTypes.bool,

  placeholder: PropTypes.string,
  options: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.oneOfType([
          PropTypes.string.isRequired,
          PropTypes.number.isRequired,
          PropTypes.shape().isRequired,
        ]),
        label: PropTypes.string.isRequired,
      })
    ).isRequired,
    PropTypes.objectOf(PropTypes.string.isRequired),
    PropTypes.objectOf(PropTypes.shape().isRequired),
  ]).isRequired,
  optionsKeys: PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  }),
  creatable: PropTypes.bool,
  removable: PropTypes.bool,
  isMulti: PropTypes.bool,
  disableSortByValue: PropTypes.bool,
  classNameReactSelect: PropTypes.string,
  classNamePrefix: PropTypes.string,
};

export default SelectField;

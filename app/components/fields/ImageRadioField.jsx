import React from "react";
import PropTypes from "prop-types";

import { getOptions } from "../../utils/OptionsParser";

const ImageRadioField = ({
  input,
  id,
  disabled,
  options,
  optionInnerHtml,
  optionsKeys,
  classNameGrid,
  classNameCol,
}) => {
  console.log(getOptions(options, optionsKeys));
  return (
    <div className={classNameGrid || "grid"}>
      {getOptions(options, optionsKeys).map(
        ({
          value,
          isHtml: optionInnerHtml = null,
          label: optionLabel = null,
          img: optionImage = null,
          imgClass: optionImageClass = null,
        }) => (
          <div key={value} className={classNameCol || "col"}>
            <input
              {...input}
              value={value}
              id={`radio_${id || (input && input.name)}_${value}`}
              disabled={disabled ? "disabled" : ""}
              type="radio"
              checked={input.value === value}
            />
            <label
              className="custom-radio"
              htmlFor={`radio_${id || (input && input.name)}_${value}`}
            >
              <span className="custom-radio-check" />
              <div className="custom-radio-content">
                {optionImage && (
                  <img
                    src={optionImage}
                    className="custom-radio-content-img"
                    alt=""
                  />
                )}
                {optionLabel &&
                  (isHtml ? (
                    <span
                      className="custom-radio-content-title"
                      dangerouslySetInnerHTML={{
                        __html: optionLabel,
                      }}
                    />
                  ) : (
                    <span className="custom-radio-content-title">
                      {optionLabel}
                    </span>
                  ))}
              </div>
            </label>
          </div>
        ),
      )}
    </div>
  );
};

ImageRadioField.propTypes = {
  input: PropTypes.shape().isRequired,
  id: PropTypes.string,
  disabled: PropTypes.bool,

  classNameGrid: PropTypes.string,
  classNameCol: PropTypes.string,
  classNameRadio: PropTypes.string,
  options: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.oneOfType([
          PropTypes.string.isRequired,
          PropTypes.number.isRequired,
          PropTypes.shape().isRequired,
        ]),
        label: PropTypes.string.isRequired,
      }),
    ).isRequired,
    PropTypes.objectOf(PropTypes.string.isRequired),
  ]).isRequired,
  optionsKeys: PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  }),
};

export default ImageRadioField;

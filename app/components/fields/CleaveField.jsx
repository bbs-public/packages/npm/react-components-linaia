import React from 'react';
import PropTypes from 'prop-types';
import Cleave from 'cleave.js/react';
import 'cleave.js/dist/addons/cleave-phone.fr';

const CleaveField = ({
  input, id, disabled, placeholder, type, cleaveOptions,
}) => (
  <Cleave
    {...input}
    id={id || (input && input.name)}
    disabled={disabled ? 'disabled' : ''}
    placeholder={placeholder}
    type={type || 'text'}
    options={cleaveOptions}
  />
);

CleaveField.propTypes = {
  input: PropTypes.shape().isRequired,
  id: PropTypes.string,
  disabled: PropTypes.bool,

  placeholder: PropTypes.string,
  type: PropTypes.string,
  cleaveOptions: PropTypes.shape(),
};

export default CleaveField;

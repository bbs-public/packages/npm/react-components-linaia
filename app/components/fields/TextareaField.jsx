import React from 'react';
import PropTypes from 'prop-types';

import WysiwygTextarea from './WysiwygTextarea';

const TextareaField = ({
  input, id, disabled, placeholder, isWysiwyg, wysiwygOptions,
}) => (isWysiwyg ? (
  <WysiwygTextarea
    text={input.value}
    onBlur={v => input.onChange(v)}
    placeholder={placeholder}
    options={wysiwygOptions}
  />
) : (
  <textarea
    {...input}
    id={id || (input && input.name)}
    disabled={disabled ? 'disabled' : ''}
    placeholder={placeholder}
  />
));

TextareaField.propTypes = {
  input: PropTypes.shape().isRequired,
  id: PropTypes.string,
  disabled: PropTypes.bool,

  placeholder: PropTypes.string,
  isWysiwyg: PropTypes.bool,
  wysiwygOptions: PropTypes.arrayOf(PropTypes.string),
};

export default TextareaField;

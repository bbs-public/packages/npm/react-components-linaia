import React from 'react';
import PropTypes from 'prop-types';

import { getOptions } from '../../utils/OptionsParser';

const RadioField = ({
  input, id, classNameOption, disabled, options, optionsKeys, classNameGroup,
}) => (
  <div className={classNameGroup || 'fields-group inline'}>
    {getOptions(options, optionsKeys).map(({ value, label: optionLabel }) => (
      <div key={value} className={classNameOption}>
        <input
          {...input}
          value={value}
          id={`radio_${id || (input && input.name)}_${value}`}
          disabled={disabled ? 'disabled' : ''}
          type="radio"
          checked={input.value === value}
        />
        <label
          className="label-option"
          htmlFor={`radio_${id || (input && input.name)}_${value}`}
        >
          {optionLabel}
        </label>
      </div>
    ))}
  </div>
);

RadioField.propTypes = {
  input: PropTypes.shape().isRequired,
  id: PropTypes.string,
  disabled: PropTypes.bool,

  classNameGroup: PropTypes.string,
  classNameOption: PropTypes.string,
  options: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({
      value: PropTypes.oneOfType([
        PropTypes.string.isRequired,
        PropTypes.number.isRequired,
        PropTypes.shape().isRequired,
      ]),
      label: PropTypes.string.isRequired,
    })).isRequired,
    PropTypes.objectOf(PropTypes.string.isRequired),
  ]).isRequired,
  optionsKeys: PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  }),
};

export default RadioField;

import React from 'react';
import PropTypes from 'prop-types';
import { Field as RFField } from 'redux-form';

import FieldLayout from './FieldLayout';

const Field = ({ component, className, ...props }) => (
  <div className={className}>
    <RFField {...props} component={FieldLayout} childComponent={component} />
  </div>
);

Field.propTypes = {
  component: PropTypes.func.isRequired,
  className: PropTypes.string
};

export default Field;

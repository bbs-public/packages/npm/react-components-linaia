import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Dropzone from "react-dropzone";
import moment from "moment";
import { get, map, filter, isEmpty } from "lodash";
import Loader from "react-loader";

import Chromestore from "../../utils/chromestore";
import {
  resizeImage,
  RESIZE_IMAGE_MAX_SIZE,
  AUTHORIZED_UPLOAD_IMAGES,
  AUTHORIZED_UPLOAD_FILES,
} from "../../utils/FileUtils";

import DefaultFileItem from "./DefaultFileItem";

export default class FileField extends React.PureComponent {
  static propTypes = {
    input: PropTypes.shape({
      onChange: PropTypes.func.isRequired,
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.arrayOf(PropTypes.string),
      ]),
    }).isRequired,
    id: PropTypes.string,
    disabled: PropTypes.bool,

    buttonLabel: PropTypes.string,
    isMulti: PropTypes.bool,
    acceptedFiles: PropTypes.arrayOf(PropTypes.string),
    base64: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.shape({
        basePath: PropTypes.string,
        baseName: PropTypes.string,
        resize: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
      }),
    ]),
    getUrl: PropTypes.func,
    renderFileBlock: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    fileBlockClassName: PropTypes.string,

    uploadFiles: PropTypes.func.isRequired,
    removeFile: PropTypes.func.isRequired,
  };

  constructor(props) {
    super();
    this.state = {
      isUploading: false,
    };
    this.isBase64 = props.base64 === true || props.base64 instanceof Object;
  }

  onDropFiles(files) {
    const {
      base64,
      isMulti,
      input: { onChange, value },
      uploadFiles,
    } = this.props;
    console.log("value of files : ", files);
    if (!files || files.length === 0) return null;

    this.setState({ isUploading: true });
    const callOnChange = (res) => {
      onChange(!isMulti ? res[0] : [...res, ...(value || [])]);
    };

    if (this.isBase64) {
      return Promise.all(
        map(files, (file, index) => {
          console.log("In this.isBase64, file : ", file);
          const filename = `${moment().format("YYYYMMDD-HHmmss")}-${get(
            base64,
            "baseName"
          )}_${index}_${file.name}`;
          const newSize =
            get(base64, "resize") === true
              ? RESIZE_IMAGE_MAX_SIZE
              : get(base64, "resize") || undefined;
          return resizeImage(
            file,
            newSize,
            filename,
            file.type
          ).then((resizedImage) =>
            Chromestore.writeBlob(get(base64, "basePath"), resizedImage)
          );
        })
      )
        .then((base64Files) => callOnChange(base64Files))
        .finally(() => this.setState({ isUploading: false }));
    }
    console.log("we call uploadFiles with files :", files);
    return uploadFiles(files)
      .then(({ response }) => callOnChange(Object.values(response)))
      .finally(() => this.setState({ isUploading: false }));
  }

  onClickRemove(element) {
    const {
      isMulti,
      input: { onChange, value },
      removeFile,
    } = this.props;
    const removeValue = () =>
      isMulti ? onChange(filter(value, (v) => v !== element)) : onChange(null);

    if (this.isBase64) {
      Chromestore.deleteFile(element.filename);
      return removeValue();
    }
    return removeFile(element).finally(() => removeValue());
  }

  renderItem(item, index) {
    const { disabled, renderFileBlock = true, getUrl } = this.props;
    if (!item) return null;
    return typeof renderFileBlock === "function"
      ? renderFileBlock(item, index, () => this.onClickRemove(item))
      : item && renderFileBlock === true && (
          <DefaultFileItem
            key={index}
            item={item}
            disabled={disabled}
            isBase64={this.isBase64}
            getUrl={getUrl}
            onClickRemove={() => this.onClickRemove(item)}
          />
        );
  }

  render() {
    const {
      input,
      id,
      buttonLabel,
      disabled,
      fileBlockClassName,
      isMulti,
      acceptedFiles = [...AUTHORIZED_UPLOAD_FILES, ...AUTHORIZED_UPLOAD_IMAGES],
    } = this.props;
    const { isUploading } = this.state;

    return (
      <div className="dropzone-block">
        <div className={fileBlockClassName || "dropzone-files-list"}>
          {isMulti
            ? map(input.value, (v, index) => this.renderItem(v, index))
            : this.renderItem(input.value)}
        </div>
        {!disabled && (isMulti || !input.value || input.value.length === 0) && (
          <Dropzone
            id={id || (input && input.name)}
            className="dropzone"
            onDrop={(files) => this.onDropFiles(files)}
            multiple={isMulti === true}
            accept={isEmpty(acceptedFiles) ? acceptedFiles.join(",") : null}
          >
            {({ getRootProps, getInputProps, isDragActive }) => (
              <div
                {...getRootProps()}
                className={classNames("dropzone", {
                  "dropzone--isActive": isDragActive,
                })}
              >
                <input {...getInputProps()} />
                <i className="fal fa-cloud-upload" />
                <span />
                <button type="button">
                  {buttonLabel || "Importer une photo"}
                </button>
              </div>
            )}
          </Dropzone>
        )}
        <Loader loaded={!isUploading} />
      </div>
    );
  }
}

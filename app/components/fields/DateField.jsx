import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import * as localeDate from 'date-fns/locale/';
import DatePicker, { registerLocale } from 'react-datepicker';
import { isString } from 'lodash';
import fr from 'date-fns/locale/fr';

registerLocale('fr', fr);

const getSelected = value => {
  if (!value) return null;
  if (isString(value)) return new Date(value);
  return value;
};

const DateField = ({
  input,
  id,
  disabled,
  placeholder,
  displayFormat = 'dd/MM/yyyy',
  locale = 'fr',
  isStartRange = false,
  selectMonthYear = false,
  isEndRange = false,
  otherRangeDate = null,
}) => (
  <Fragment>
    <DatePicker
      selected={getSelected(input.value)}
      onChange={d => input.onChange(d)}
      id={id || (input && input.name)}
      disabled={disabled}
      placeholderText={placeholder}
      dateFormat={displayFormat}
      className="datepicker"
      autoComplete="off"
      locale={localeDate[locale]}
      showMonthYearPicker={selectMonthYear || isStartRange || isEndRange}
      selectsStart={isStartRange}
      selectsEnd={isEndRange}
      startDate={
        isStartRange
          ? getSelected(input.value)
          : isEndRange
          ? otherRangeDate
          : false
      }
      endDate={
        isEndRange
          ? getSelected(input.value)
          : isStartRange
          ? otherRangeDate
          : false
      }
    />
    {input.value && (
      <i
        className="far fa-times input-cross"
        onClick={() => input.onChange(null)}
      />
    )}
  </Fragment>
);

DateField.propTypes = {
  input: PropTypes.shape().isRequired,
  id: PropTypes.string,
  disabled: PropTypes.bool,
  locale: PropTypes.shape(),
  placeholder: PropTypes.string,
  displayFormat: PropTypes.string,
};

export default DateField;

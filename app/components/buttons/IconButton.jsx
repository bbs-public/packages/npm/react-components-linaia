import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './icons-button.scss';

const IconButton = ({
  icon, fontIcon, fontIconPrefix = 'fi', title, className, onClick,
}) => (
  <button
    onClick={onClick || null}
    className={className || 'btn'}
    title={title}
    type="button"
  >
    { fontIcon
      ? <i className={classNames(fontIconPrefix, fontIcon)} />
      : <span className={classNames('bt-action-icon', icon)} />
    }
  </button>
);

IconButton.propTypes = {
  icon: PropTypes.string,
  fontIcon: PropTypes.string,
  fontIconPrefix: PropTypes.string,
  title: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

export default IconButton;

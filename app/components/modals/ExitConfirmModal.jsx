import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';

import ConfirmationModal from './ConfirmationModal';

export default class ExitConfirmModal extends React.PureComponent {
  static propTypes = {
    ...Modal.propTypes,
    children: PropTypes.node,
  };

  constructor (props) {
    super();
    this.state = {
      confirmationBox: null,
    };

    this.showConfirmationBox = () => {
      if (props.disabled === true) {
        return this.closeModal();
      }
      return this.setState({ confirmationBox: true });
    };
    this.hideConfirmationBox = () => this.setState({ confirmationBox: null });
    this.closeModal = () => {
      const { onRequestClose } = this.props;
      this.hideConfirmationBox();
      onRequestClose();
    };
  }

  render () {
    const { confirmationBox } = this.state;
    // Copy initial props, and replace onRequestClose with ExitConfirmModal
    const modalProps = {
      ...this.props,
      onRequestClose: () => this.showConfirmationBox(),
    };
    // Set default className / overlayClassName if not exists
    if (!modalProps.className) {
      modalProps.className = {
        base: 'modal-content',
        afterOpen: 'small-modal',
        beforeClose: 'small-modal',
      };
    }
    if (!modalProps.overlayClassName) {
      modalProps.overlayClassName = {
        base: 'modal-overlay',
        afterOpen: 'modal-overlay',
        beforeClose: 'modal-overlay',
      };
    }
    // Remove children from First Modal (Children will be the code bellow with the second Modal)
    delete modalProps.children;
    // If onRequestClose exist in the children, replace it with the new onRequestClose
    let { children } = this.props;
    if (children && children.props
      && children.props.onRequestClose === true) {
      children = React.cloneElement(children, {
        onRequestClose: this.showConfirmationBox,
      });
    }
    return (
      <Modal
        {...modalProps} // Propagate copied props
      >
        {/* Render the initial child */}
        {children}
        {/* Add the second Modal (Confirmation) to the children of the first Modal */}
        <ConfirmationModal
          isOpen={confirmationBox !== null}
          onRequestClose={() => this.hideConfirmationBox()}
          onConfirmationButton={() => this.closeModal()}
          message="Êtes-vous sûr(e) de vouloir quitter la fenêtre ?"
        />
      </Modal>
    );
  }
}

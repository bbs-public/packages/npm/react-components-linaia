export function validateEmail (string) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(string);
}

export function validatePostalCode (string) {
  return /^[0-9]{5}$/.test(string);
}

export function validatePhone (string) {
  return /^[0-9 .\-+]+$/.test(string);
}

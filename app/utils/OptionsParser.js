import { map } from 'lodash';

export function parseObjectOptions (options, optionsKeys) {
  return map(options, (label, value) => ({
    label: optionsKeys && optionsKeys.label ? label[optionsKeys.label] : label,
    value: optionsKeys && optionsKeys.value ? label[optionsKeys.value] : value,
  }));
}

export function getOptions (options, optionsKeys) {
  if (Array.isArray(options)) {
    return options;
  }
  return parseObjectOptions(options, optionsKeys);
}

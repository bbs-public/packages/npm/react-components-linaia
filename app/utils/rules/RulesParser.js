import {
  filter, each, find, result, cloneDeep,
} from 'lodash';

import FunctionOperator from './FunctionOperators';

import { getResponsePath } from '../StringUtils';

/* evalConditionElement :
 * Evaluate an expression element with the given source and the given target
 * Params :
 * expression: {
 *   operator: the operator of the expression
 *   source: the source element of the expression
 *   target: the target element of the expression
 * }
 * Return: boolean (true / false)
 */
export function evalConditionElement (expression) {
  // TODO : maybe check that the 'source' is required
  // but the 'target' can be undefined (for EMPTY / NOT_EMPTY)
  if (typeof FunctionOperator[expression.operator] !== 'function') {
    // Unknow operator
    throw String('Unknow operator...');
  }
  return FunctionOperator[expression.operator](expression.source, expression.target);
}

/* evalExpression
 * Evaluate each expressions in the group and concatenate results with the given group operator
 * Params :
 * expression: {
 *   operator: the operator of the group
 *   expressions: Array of expressions
 * }
 * Return: boolean (true / false)
 */
export function evalExpression (expressionGroup) {
  const groupOperator = expressionGroup[0];

  if (groupOperator !== 'AND' && groupOperator !== 'OR') {
    // The groupOperator is invalid
    throw String('Bad arguments : unknow operator');
  }

  if (!Array.isArray(expressionGroup) || expressionGroup.length < 3) {
    // The group must contains at least two elements to compare
    throw String('Bad arguments : expression must be an array with 2 or more elements and one operator at index 0');
  }

  let res;
  for (let i = 1; i < expressionGroup.length; i++) {
    if (Array.isArray(expressionGroup[i])) {
      // expressionGroup[i] is a group
      res = evalExpression(expressionGroup[i]);
    } else if (expressionGroup[i] === true || expressionGroup[i] === false) {
      // expressionGroup[i] is already a boolean
      res = expressionGroup[i];
    } else {
      // expressionGroup[i] is an element
      res = evalConditionElement(expressionGroup[i]);
    }

    if ((groupOperator === 'AND' && res === false) || (groupOperator === 'OR' && res === true)) {
      // Stop iteration here, and return res
      break;
    }
  }

  return res;
}

/* evaluate
 * Evaluate a property (boolean, expression...)
 */
export function evaluateRule (property) {
  return (Array.isArray(property)) ? evalExpression(property) : property;
}

/* transformExpression :
 * Update the expression by replacing questions identifiers by their actuals responses
 * Params :
 * expression: the given expression to transform
 * questions: the list of questions with responses
 * Return: the updated expression
 */
export function transformExpression (expression, questions, sRIndex = null, gRIndex = null) {
  const cloneExpression = cloneDeep(expression);
  const elements = filter(cloneExpression, (item) => {
    if (Array.isArray(item)) {
      item = this.transformExpression(item, questions, extractProperty, sRIndex, gRIndex);
      return false;
    }
    return (item.source && item.source.startsWith('$'));
  });
  each(elements, (element) => {
    const sourceQuestion = questions[element.source.substring(1)];
    if (!sourceQuestion) {
      console.error(`No question found for the id ${element.source.substring(1)}`);
    }

    element.source = getResponsePath(sRIndex, gRIndex, sourceQuestion);
    if (typeof(element.target) === 'string' && element.target.startsWith('$')) {
      element.target = result(find(result(sourceQuestion, 'values'), { id: parseInt(element.target.substring(1), 0) }), 'libelle');
    }
  });
  return cloneExpression;
}

import { toString } from 'lodash';

export default {
  EQUALS: (source, target) => toString(source) === toString(target),
  INEQUAL: (source, target) => toString(source) !== toString(target),
  // GREATER: (source, target) =>
  //   parseFloat(source) > parseFloat(target),
  // LESS: (source, target) =>
  //   parseFloat(source) < parseFloat(target),
  // GREATER_EQUAL: (source, target) =>
  //   parseFloat(source) >= parseFloat(target),
  // LESS_EQUAL: (source, target) =>
  //   parseFloat(source) <= parseFloat(target),
  CONTAINS: (source, target) => (source !== undefined)
    && (source !== null)
    && ((source.includes && source.includes(target))
      || (!source.includes && source.toString().includes(target))),
  NO_CONTAINS: (source, target) => (source === undefined)
    || (source === null)
    || ((source.includes && !source.includes(target))
      || (!source.includes && !source.toString().includes(target))),
  EMPTY: source => (source === undefined)
    || (source === null)
    || ((source.length && source.length === 0)
      || (!source.length && source.toString().length === 0)),
  NOT_EMPTY: source => (source !== undefined)
    && (source !== null)
    && ((source.length && source.length > 0)
      || (!source.length && source.toString().length > 0)),
  // STARTS_WITH: (source, target) =>
  //   (source !== undefined) && source.toString().startsWith(target),
  // ENDS_WITH: (source, target) =>
  //   (source !== undefined) && source.toString().endsWith(target),
  // ADD: (source, target) => {
  //   if (source && source.constructor && source.constructor.name === 'Moment') {
  //     source.add(parseInt(target, 10), 'days');
  //     return source.format('DD/MM/YYYY');
  //   }
  //   return parseFloat(source) + parseFloat(target);
  // },
  // SUBSTRACT: (source, target) => {
  //   if (source && source.constructor && source.constructor.name === 'Moment') {
  //     if (target && target.constructor && target.constructor.name === 'Moment') {
  //       return source.diff(target, 'days');
  //     }
  //     source.subtract(parseInt(target, 10), 'days');
  //     return source.format('DD/MM/YYYY');
  //   }
  //   return parseFloat(source) - parseFloat(target);
  // },
  // MULTIPLIES: (source, target) =>
  //   parseFloat(source) * parseFloat(target),
  // DIVIDES: (source, target) => {
  //   if (!target) {
  //     console.error('Divide by 0 is not allowed');
  //     return '';
  //   }
  //   return parseFloat(source) / parseFloat(target);
  // },
  // CONCAT: (source, target) =>
  //   source ? String(source).concat(target) : target,
  // DUPLICATE: source => source,
};

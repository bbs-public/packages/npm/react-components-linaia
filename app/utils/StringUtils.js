export const OPT_COURTESY_TITLE = {
  mr: { short: 'M.', long: 'Monsieur' },
  mme: { short: 'Mme', long: 'Madame' },
};

export function upperFirst (string) {
  return string[0].toUpperCase() + string.substr(1);
}

export function pad (source, length, char = '0') {
  const string = source.toString();
  return string.length < length ? pad(char + string, length, char) : string;
}

export function displayCourtesy (person = {}, type = 'short', courtesyTitle = OPT_COURTESY_TITLE) {
  return courtesyTitle[person.courtesy_title]
    ? courtesyTitle[person.courtesy_title][type]
    : '';
}

export function displayFullname (person = {}, showCourtesy = true) {
  const fullName = `${person.lastname || ''} ${person.firstname || ''}`;
  if (showCourtesy) {
    return `${displayCourtesy(person)} ${fullName}`;
  }
  return fullName;
}

export function getPhoneLink (phone) {
  return phone && `tel:${phone.replace(/\s/g, '')}`;
}

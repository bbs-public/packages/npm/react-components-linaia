# React Components Linaïa

## Running the project

### Staging
To build for staging, this command will output optimized staging code:

```bash
yarn run staging
```

### Production
To build for production, this command will output optimized production code:

```bash
yarn run production
```

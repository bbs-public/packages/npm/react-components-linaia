import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import ButtonConfirmModal from '../app/components/modals/ButtonConfirmModal';
import notes from './ButtonConfirmModal.md';

storiesOf('ButtonConfirmModal', module)
  .add('utilisation', () => (
    <ButtonConfirmModal
      message="Pour confirmer, cliquez sur OUI"
      onConfirmationButton={action('confirm')}
    >
      <h6>Cliquez ici pour lancer la modal</h6>
    </ButtonConfirmModal>
  ), {
    notes,
  });

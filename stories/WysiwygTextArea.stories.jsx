import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import WysiwygTextArea from '../app/components/fields/WysiwygTextarea';
import notes from './WysiwygTextArea.md';

storiesOf('WysiwygTextArea', module)
  .add('utilisation', () => (
    <WysiwygTextArea
    text= ""
    icon=""
    placeholder= ""
    onBlurProps={e => {
      return change('content', e);
    }}
    options={["embedded","list"]}
    toolbarOptions= {{
      inline: {
        options: ['bold', 'italic', 'underline']
      }
    }}
    />
  ), {
    notes,
  });

# Composant : WysiwygTextArea

Le composant affiche un Wysiwyg :

*Props : *

 * text: PropTypes.string,
 * placeholder: PropTypes.string,
 * onBlurProps: PropTypes.func.isRequired,
 * options: PropTypes.arrayOf(PropTypes.string),
 * toolbarOptions: PropTypes.shape(),



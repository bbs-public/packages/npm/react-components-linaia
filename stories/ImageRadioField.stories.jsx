import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
// import { reduxForm, reset } from "redux-form";
import withReduxForm from "redux-form-storybook";

import ImageRadioField from "../app/components/fields/ImageRadioField";
import notes from "./ImageRadioField.md";

storiesOf("ImageRadioField", module)
  .addDecorator(withReduxForm)
  .add(
    "utilisation",
    () => (
      <form className="" name="form">
        <ImageRadioField
          input={{ value: true }}
          options={[
            {
              value: true,
              img:
                "https://krys.linaia.online/api/uploads/20200310-174128-logo-kalixia-color.png"
            },
            {
              value: false,
              label: "test2",
              img:
                "https://krys.linaia.online/api/uploads/20200310-174151-logo-seveane-groupama-gan.png"
            }
          ]}
        ></ImageRadioField>
      </form>
    ),
    {
      notes
    }
  );

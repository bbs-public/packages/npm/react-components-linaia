import React from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import ExitConfirmModal from '../app/components/modals/ExitConfirmModal';
import notes from './ExitConfirmModal.md';

const DummyForm = ({ onRequestClose, handleSubmit }) => (
 <div className="grid-noGutter full-height">
  <div className="col grid-column grid-noGutter">
    <div className="col flex-0">
      <div className="modal-title">Dummy form {onRequestClose && <button className="btn-close-modal" onClick={onRequestClose}><i className="fi fi-cross-circle" /></button>}</div>
    </div>
    <form onSubmit={handleSubmit}>
    </form>
  </div>
  </div>
);
DummyForm.propTypes = {
  onRequestClose: PropTypes.func,
  handleSubmit: PropTypes.func.isRequired,
};

storiesOf('ExitConfirmModal', module)
  .addDecorator(withKnobs)
  .add('not disabled', () => {
    return (
      <div>
        <ExitConfirmModal
          disabled={false}
          isOpen={boolean('Open', true)}
          onRequestClose={action('requestClose')}
        >
          <DummyForm
            onSubmit={() => action('onSubmit')}
            onRequestClose
          />
        </ExitConfirmModal>
      </div>
    );
  } , {
    notes,
  })
  .add('disabled', () => {
    return (
      <div>
        <ExitConfirmModal
          disabled={true}
          isOpen={boolean('Open', true)}
          onRequestClose={action('requestClose')}
        >
          <DummyForm
            onSubmit={() => action('onSubmit')}
            onRequestClose
          />
        </ExitConfirmModal>
      </div>
    );
  } , {
    notes,
  });;

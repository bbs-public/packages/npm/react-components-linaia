# Composant : ButtonConfirmModal

Le composant affiche le message passé en props et deux boutons :

* NON : pour fermer la fenêtre
* OUI : pour lancer onConfirmationButton

*Props : *

* message : string, requis
* onConfirmationButton : func, requis
* children : node, requis



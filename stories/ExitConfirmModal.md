# Composant : ExitConfirmModal

Le composant gère la confirmation de la fermeture d'une modal avec le composant ButtonConfirmModal

La confirmation n'est pas demandée si la props disabled est à true.

Comme il s'agit d'une modal, elle prend en props toujours les propriétés d'une modal : isOpen, onRequestClose ...

*Props : *

* children : node, requis
* onRequestClose : func, optional
* disabled: boolean, optional



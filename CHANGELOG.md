# 0.0.5 - 29/03/2019

## BREAKING CHANGES
* DateField
Remove prop 'dateFormat' : Remove prop 'dateFormat'. DateField return now a Date object (with time)

## Features
* ToggleField
Add new field ToggleField : Add ToggleField for boolean values.

# 0.0.4 - 27/03/2019

## BREAKING CHANGES
* MultiCheckboxField, RadioField
Rename prop 'className' to 'classNameOption' : The className of an option was renamed from 'className' to 'classNameOption'.

## Features
* InputField
Parse number types : If type="number", the returned is now an Interger or a Float.

## Fix
* All Fields
Fix 'disabled' & 'id' props to component fields.

# 0.0.3 - 26/03/2019

## BREAKING CHANGES
* Field, ArrayField
Add div container : A new div surround the Field / FieldArray. ClassName of Field & ArrayField from ReactComponentLinaïa will always have the "field" "field field-array" class.

* FileField
Font Awsome : Use new font awesome version (icon fal fa-cloud-upload)

## Features
* Field, ArrayField
Add optional props : 'className'. Can customize className of the new div container.

# 0.0.2 - 13/03/2019

## BREAKING CHANGES
* FileField
Remove optional props : 'uploadBaseURL'. See the new 'getUrl' method bellow to replace it ;)

## Features
* FileField
Add optional props : 'getUrl'. Let to add a function that return the URL of the item given in params.

* MultiCheckboxField, RadioField, SelectField
Add optional props : 'optionsKeys'. Let to define the key of 'value' & 'label' props in an options object.

* DateField
Add optional props : 'displayFormat'. Let to define the display format, which can differ from dateFormet used for value formating.

# 0.0.1 - 17/01/2019

* Création du projet
